# Build

docker login gitlab-research.centralesupelec.fr:4567

docker build -t gitlab-research.centralesupelec.fr:4567/marcadet/sip_gitlab_ci_docker_image .

docker push gitlab-research.centralesupelec.fr:4567/marcadet/sip_gitlab_ci_docker_image
