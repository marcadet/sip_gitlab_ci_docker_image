import inspect

import hello

TXT_BRIGHT_RED="\x1b[91m"
TXT_BRIGHT_GREEN="\x1b[92m"
TXT_BRIGHT_CYAN="\x1b[96m"
TXT_CLEAR="\x1b[0m"

def check_hello_function():
    print("  >> " + TXT_BRIGHT_CYAN + "INFO: checking existence of function hello() with one argument" + TXT_CLEAR)
    if not "hello" in dir(hello):
        print("  >> " + TXT_BRIGHT_RED + "ERROR: you must define a function named hello()" + TXT_CLEAR)
        return False
    if not callable(hello.hello):
        print("  >> " + TXT_BRIGHT_RED + "ERROR: you must define a function named hello()" + TXT_CLEAR)
        return False
    count = len(inspect.signature(hello.hello).parameters)
    if count < 1:
        print("  >> " + TXT_BRIGHT_RED + "ERROR: your function named hello() must take at least 1 argument" + TXT_CLEAR)
        return False
    print("  >> " + TXT_BRIGHT_GREEN + "SUCCESS: you have one function hello() with at least one argument" + TXT_CLEAR)
    return True


if __name__ == "__main__":
    if not check_hello_function():
        exit(-1)
    exit(0)

